import seabreeze
seabreeze.use('cseabreeze')
from seabreeze.spectrometers import list_devices, Spectrometer
import time
import struct


def init_seabreeze():
    global devices
    devices = []
    for i in list_devices():
        device_name = str(i)[-9:][:-1]
        devices.append(device_name)
    return devices


def get_spectra_from_device(device, channel=0):
    spec = Spectrometer.from_serial_number(device)
    spec.f.raw_usb_bus_access.raw_usb_write(data=struct.pack('<BB', 0xC1, channel), endpoint='primary_out')
    time.sleep(.1)  # Not necessary?
    wavelengths = list(spec.wavelengths())
    intensities = list(spec.intensities())
    reset_channel(spec)
    spec.close()
    return wavelengths, intensities
    

def reset_channel(spec):
    channel = 0
    spec.f.raw_usb_bus_access.raw_usb_write(data=struct.pack('<BB', 0xC1, channel), endpoint='primary_out')
