# B12 i2c Library v0.8c - 20210512
# Author Evan Taylor - Evan@EvanTaylor.Pro
# i2c PCA9506 40bit I/O Muxxer or i2c device 0x26
# Command Address byte is 0100110 or 0x98 or 0x88 for reads
#cython: language_level=3
import time
import math
import os

# Micropython Specific Code
if os.uname()[0] == 'rp2':
    import machine
    import struct

    sda = machine.Pin(14)
    scl = machine.Pin(15)
    i2c = machine.SoftI2C(sda=sda, scl=scl, freq=400000)

    class bus():
        def read_byte_data(addr, register):
            """ Read a single byte from register of device at addr
                Returns a single byte """
            return i2c.readfrom_mem(addr, register, 1)[0]

        def read_i2c_block_data(addr, register, length):
            """ Read a block of length from register of device at addr
                Returns a bytes object filled with whatever was read """
            return i2c.readfrom_mem(addr, register, length)

        def write_byte_data(addr, register, data):
            """ Write a single byte from buffer `data` to register of device at addr
                Returns None """
            # writeto_mem() expects something it can treat as a buffer
            data = bytes(data)
            return i2c.writeto_mem(addr, register, data)

        def write_i2c_block_data(addr, register, data):
            """ Write multiple bytes of data to register of device at addr
                Returns None """
            # writeto_mem() expects something it can treat as a buffer
            return i2c.writeto_mem(addr, register, data)
        
        def xprint(*args):
            '''Print Wrapper to add timestamp and hostname to print or tqdm.write() function.'''
            time.localtime()
            time_string = str(time[0]) + "-" + str(time[1]) + "-" + str(time[2]) + " " + str(time[3]) + ":" + str(time[4] + ":" + str(time[5] + "." + str(time[6])))
            timestamp = str("[" + time_string + "]")
            prefix = timestamp
            print(prefix, *args)
# End Micropython Specific Code
else:
    from smbus2 import SMBus
    bus = SMBus(1)
    # Timestamp/tqdm print wrapper, useful for psuedo-benchmarking
    def xprint(*args, **kwargs):
        '''Print Wrapper to add timestamp and hostname to print or tqdm.write() function.'''
        import datetime
        from tqdm import tqdm
        timestamp = str("[" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f") + "]")
        prefix = timestamp  # + hostname
        try:
            tqdm.write(prefix, *args, **kwargs)
        except Exception:
            print(prefix, *args, **kwargs)


# Useful for reading the bit order of the mux bit arrays
def print_binary_array(array):
    string_array = []
    for i in array:
        string_array.append(zfl(str(bin(i))[2:], 8))
    string_array = str(string_array)
    xprint(string_array)


def zfl(s, width):
    return '{:0>{w}}'.format(s, w=width)


##########################################################################################
# Functions for porting to C and related variables are below
# God help you regarding the bit manipulation function I wrote.  Should be redone as byte arrays.
##########################################################################################


# i2c Bus Variables
io_addr = 0x26
io_comm_write_addr = 0x98  # IO chip documentation requires this address for initialization.
io_comm_read_addr = 0x88  # IO chip documentation is stupid use this address to write Mux changes.
ad_addr = 0x14

# 40 bit IO byte array default state
# Everything is off
# This array will be updated by which muxxing setup is required for A/D read
# Eventually, Evan will write a state parser for diagnostic/testing purposes.
state_array = [0x00, 0x00, 0x00, 0xf0, 0x00]

# Timing Variables
reading_ms = 50/1000
mux_ms = 20/1000
interval_ms = 40/1000

# read_ad base precision
ad_count = 5  # this sets the default AD to 5 + 1 read

# Calibration/Offset Variables
voltage_divisor = 12.6


# Modify to only adjust 3 bits, due to reasons.
def das_blinken_lights(colors):
    """Accepts, True/False, or string combos of 'ryg' to switch LEDs"""
    # 40 bi I/O Muxxer Byte Array
    # [Port 0, Port 1, Port 2, Port 3, Port 4]
    # Only the first 4 bytes of port 3 are related to LED functionality
    # Default Byte Array in LED OFF configuration
    global state_array
    state_array[0] = 0x00
    # Assuming you have to use the io_comm_write_addr register.
    if colors is False:
        state_array[3] = 0xf0
        set_mux(state_array)
    elif colors is True:
        state_array[3] = 0x80
        set_mux(state_array)
    else:
        colors = colors.lower()
        colors = colors.replace('green', 'g')
        colors = colors.replace('red', 'r')
        colors = colors.replace('yellow', 'y')
        if all(x in colors for x in ['g', 'r', 'y']):
            state_array[3] = 0x80
            set_mux(state_array)
        elif "y" in colors and "r" in colors:
            state_array[3] = 0x40
        elif "y" in colors and "g" in colors:
            state_array[3] = 0x20
        elif "g" in colors and "r" in colors:
            state_array[3] = 0x90
        elif "g" in colors:
            state_array[3] = 0x30
        elif "y" in colors:
            state_array[3] = 0x60
        elif "r" in colors:
            state_array[3] = 0x50
        set_mux(state_array)


def test_lights():
    import random
    lights = ['r', 'y', 'g', 'ryg', 'ry', 'gr', 'yg', False]

    for i in range(10):
        das_blinken_lights(random.choice(lights))
        time.sleep(20/1000)


class Custom_Error(Exception):
    pass


# God awful nibble modification function.  There's got to be a better way.
def modify_byte(byte_value, low_nibble=False, high_nibble=False):
    '''Pass IO byte current value, high and low nibble modifications.'''
    # Make sure Byte and optional nibble error checking
    if low_nibble is False and high_nibble is False:
        raise Custom_Error("Error: You must pass at least one nibble.")
    # God awful way of doing this.
    if len(str(bin(byte_value))) > 10:
        raise Custom_Error("Error: Byte longer than 8 bits.")
    # Sometimes I wonder how this logic is my normal way of thinking.
    if high_nibble is not False and len(str(bin(high_nibble))) > 6:
        raise Custom_Error("Error: High Nibble longer than 4 bits.")
    # Really, there's not a better way?
    if low_nibble is not False and len(str(bin(low_nibble))) > 6:
        raise Custom_Error("Error: Low Nibble longer than 4 bits.")

    # Parse current state nibbles
    byte_value = zfl(bin(byte_value)[2:], 8)
    byte_value_low_nibble = byte_value[4:]
    byte_value_high_nibble = byte_value[0:4]

    # Optionally modify low nibble
    if low_nibble is not False:
        low_nibble = bin(low_nibble)[2:]
        byte_value_low_nibble = zfl(low_nibble, 4)

    # Optionally modify high nibble
    if high_nibble is not False:
        high_nibble = bin(high_nibble)[2:]
        byte_value_high_nibble = zfl(high_nibble, 4)
    # Reconstruct modified or unmodified nibbles into a full byte
    modified_byte = byte_value_high_nibble + byte_value_low_nibble
    modified_byte = bytes(modified_byte, 'utf-8')
    # Convert byte string to usable integer
    modified_byte = int(modified_byte, 2)

    return modified_byte


# Must call this function at startup or power/reset of the board.
def init_i2c_mux():
    bus.write_i2c_block_data(io_addr, 0x98, bytes([0x00, 0x00, 0x00, 0x00, 0x00]))


# Ignore this for now.  This theoretically improved precision in a way that just doesn't matter.
def init_i2c_ad(hz=30):
    '''Set hz equal to operating modes 30 or 60.  Default: hz=60'''
    # leave this alone. May not even need it.
    hz = hz
    if hz == 60:
        # Write Request
        bus.write_byte_data(i2c_addr=ad_addr, register=0x28, value=0x01)
    elif hz == 30:
        # Write Request
        bus.write_byte_data(i2c_addr=ad_addr, register=0x28, value=0x00)
    else:
        raise Custom_Error("Error: LTC2451-1 - You must set hz=30 or hz=60.")


def set_mux(command):
    command = bytes(command)
    bus.write_i2c_block_data(io_addr, io_comm_read_addr, command)
    time.sleep(reading_ms)


def reset_mux():
    global state_array
    command = state_array
    command[3] = modify_byte(command[3], low_nibble=0x00)
    set_mux(command)
    time.sleep(mux_ms)


def read_ad_raw():
    # Don't use this unless you don't want oversampled measurements
    raw_data = bus.read_i2c_block_data(ad_addr, 0x29, 2)
    data = (raw_data[0] * 256) + raw_data[1]
    return data


def read_ad(count=ad_count, interval_ms=interval_ms):
    '''Oversampling i2c bus AD read.  Does not handle < 35ms on an rPi.'''
    '''Provides average of middle 3 or more measurements.'''
    interval_ms = float(round(interval_ms, 4))  # Prevents some divide by zero errors.
    count = int(ad_count)  # This should not be necessary.
    ad_value = []
    # Adds an extra reading to remove later due AD buffer/switching.
    count = count + 1
    for i in range(count):
        try:
            raw_data = bus.read_i2c_block_data(ad_addr, 0x29, 2)
            data = (raw_data[0] * 256) + raw_data[1]
            # Ignores potentially bad data from initial reading.
            if i > 0:
                ad_value.append(data)
            time.sleep(interval_ms)
        except Exception:
            continue
    total_reads = len(ad_value)
    if total_reads > 10:
        threshold = int(count * (1 - .8664))  # Psuedo-normal distribution 1.5 sigma filter
    if total_reads <= 10:
        threshold = 1
    ad_value.sort()
    # Apply 1.5+ sigma filter
    ad_value = ad_value[threshold:-threshold]
    filtered_reads = len(ad_value)
    # Average remaining measurements
    data = sum(ad_value) / filtered_reads
    return data


def get_cathode_raw():
    # Setup IO Mux
    global state_array
    low_nibble = 0x02
    state_array[3] = modify_byte(byte_value=state_array[3], low_nibble=low_nibble)
    mux_command = state_array
    set_mux(mux_command)
    # Read AD
    cathode_raw = read_ad()
    x = cathode_raw
    return x


def get_solar_voltage():
    # Setup IO Mux
    global state_array
    low_nibble = 0x03
    state_array[3] = modify_byte(byte_value=state_array[3], low_nibble=low_nibble)
    mux_command = state_array
    set_mux(mux_command)
    # Read AD
    solar_voltage_raw = read_ad()
    x = solar_voltage_raw / 2303.14
    solar_voltage = x
    solar_voltage = round(solar_voltage, 2)
    return solar_voltage


def get_battery_voltage():
    # Setup IO Mux
    global state_array
    low_nibble = 0x04
    state_array[3] = modify_byte(byte_value=state_array[3], low_nibble=low_nibble)
    mux_command = state_array
    set_mux(mux_command)
    # Read AD
    voltage_raw = read_ad()
    x = voltage_raw / 2303.14
    battery_voltage = x
    battery_voltage = round(battery_voltage, 2)
    return battery_voltage


def get_battery_current():
    # Setup IO Mux
    global state_array
    low_nibble = 0x05
    state_array[3] = modify_byte(byte_value=state_array[3], low_nibble=low_nibble)
    mux_command = state_array
    set_mux(mux_command)
    # Read AD
    current_raw = read_ad()
    x = current_raw - 32824
    battery_current = x / 11835.0
    battery_current = round(battery_current, 2)
    return battery_current


def get_board_temp():
    # Setup IO Mux
    global state_array
    low_nibble = 0x06
    state_array[3] = modify_byte(byte_value=state_array[3], low_nibble=low_nibble)
    mux_command = state_array
    set_mux(mux_command)
    # Read AD
    board_temp_raw = read_ad()
    # MCP9700A Conversion
    x = board_temp_raw / 25.21
    x = x - 500
    temp_c = x / 10
    temp_c = round(temp_c, 2)
    return temp_c


def get_cathode_leak():
    # Setup IO Mux
    global state_array
    low_nibble = 0x07
    state_array[3] = modify_byte(byte_value=state_array[3], low_nibble=low_nibble)
    mux_command = state_array
    set_mux(mux_command)
    # Read AD
    cathode_leak_raw = read_ad()
    x = 65536 - cathode_leak_raw
    x = cathode_leak_raw / x
    x = x * 0.2
    cathode_leak = x - 0.01
    cathode_leak = round(cathode_leak, 2)
    return cathode_leak


def get_anode_raw(anode):
    # Human-readable anode number parsed to i2c byte anode number:
    anode = anode - 1
    anode = str('0x0') + str(anode)
    anode = int(anode, 16)
    anode = anode + 8  # Inhibits ORP read

    # Setup IO Mux
    global state_array
    state_array[0] = modify_byte(byte_value=state_array[0], high_nibble=0x00, low_nibble=anode)
    state_array[3] = modify_byte(byte_value=state_array[3], low_nibble=0x00)
    mux_command = state_array
    set_mux(mux_command)
    # Read Anode
    anode_raw = read_ad()
    return anode_raw


def anode_potential(anode_raw, cathode_raw):
    # Convert to Calibrated Potential Voltage
    # This function will be updated with higher precision calibration calculations
    x = anode_raw
    y = cathode_raw
    z = x - y
    z = z / voltage_divisor
    z = round(z, 2)
    return z


def get_anode_voltage_single(anode):
    if anode > 0 and anode < 9:
        anode_raw = get_anode_raw(anode)
        cathode_raw = get_cathode_raw()
    else:
        raise Custom_Error("Invalid Anode Number.")
    anode_voltage = anode_potential(anode_raw, cathode_raw)
    reset_mux()
    return anode_voltage


# Still investigating the timing/reading on this.  Does not behave as well as the individual reads.
def get_anode_voltage_all():
    '''Experimental High-speed anode read function.  For testing purposes only.'''
    readings = []
    # Single Cathode Read, to save time vs doing indivual node/cathode reads.
    cathode_raw = get_cathode_raw()
    for i in range(1, 9):
        anode_raw = get_anode_raw(i)
        anode_voltage = anode_potential(anode_raw, cathode_raw)
        readings.append(anode_voltage)
    reset_mux()
    return readings


def get_aux_voltage_raw(aux):
    # Human-readable anode number parsed to i2c byte anode value.
    aux = aux - 1
    aux = str('0x0') + str(aux)
    aux = int(aux, 16)
    aux = aux
    anode_inhibit = 0x01  # Inhibits Anode Read

    # Setup IO Mux
    global state_array
    state_array[0] = modify_byte(byte_value=state_array[0], high_nibble=anode_inhibit, low_nibble=aux)
    state_array[3] = modify_byte(byte_value=state_array[3], low_nibble=0x00)
    mux_command = state_array
    set_mux(mux_command)
    # Read Anode
    aux_raw = read_ad()
    return aux_raw


def get_aux_voltage_single(aux):
    if aux > 0 and aux < 9:
        aux_negative = get_aux_voltage_raw(aux)
        aux_2 = int(aux + 4)  # ORP/pH Probes have their own reference.
        aux_positive = get_aux_voltage_raw(aux_2)
    else:
        raise Custom_Error("Invalid ORP/pH port.  Accepts Values 1-4 for ORP+ and 5-8 for ORP- ")
    potential = aux_positive - aux_negative
    aux_voltage = potential / voltage_divisor
    aux_voltage = round(aux_voltage, 2)
    reset_mux()
    return aux_voltage


def get_aux_voltage_all():
    readings = []
    for i in range(1, 5):
        readings.append(get_aux_voltage_single(i))
    reset_mux()
    return readings


def steinhart_equation(ohms, thermistor_raw, beta, To, Ro):
    # Equation Coefficients
    To = To
    Ro = Ro
    r = ohms / ((65535/thermistor_raw) - 1)
    steinhart = math.log(r / Ro) / beta
    steinhart = steinhart + 1.0 / (To + 273.15)
    steinhart = (1.0 / steinhart) - 273.15
    steinhart = round(steinhart, 2)
    return steinhart


def get_thermistor_single(thermistor, therm_range='mid'):
    # Human-readable thermistor numbers parsed to i2c byte value.
    thermistor = thermistor - 1
    low_nibble = 0x00
    high_nibble = 0x00

    To = 25.0
    Ro = 10000.0
    # Get beta and Ohms for each thermistor curve
    beta = 3890  # Change this to specific beta for each "class" of thermistor
    if therm_range == 'hot':
        low_nibble = 0x00
        ohms = 2018
    if therm_range == 'mid':
        low_nibble = 0x08
        ohms = 10078
        beta = 3950
    if therm_range == 'cool':
        high_nibble = 0x01
        ohms = 40218
    if therm_range == 'cold':
        high_nibble = 0x02
        ohms = 162018

    low_nibble = low_nibble + thermistor
    # Setup IO Mux
    global state_array
    # Setup Thermistor Mux
    state_array[2] = modify_byte(byte_value=state_array[2], high_nibble=high_nibble, low_nibble=low_nibble)
    state_array[3] = modify_byte(byte_value=state_array[3], low_nibble=0x01)
    # Inhibit 4-20mA Loop / Volt Reads
    state_array[4] = modify_byte(byte_value=state_array[4], low_nibble=0x08)
    set_mux(command=state_array)
    thermistor_raw = read_ad()
    temp_c = steinhart_equation(ohms, thermistor_raw, beta, To, Ro)
    reset_mux()
    return temp_c


def get_thermistor_all(therm_range='mid'):
    temps = []
    therm_range = therm_range
    for i in range(1, 9):
        temps.append(get_thermistor_single(i, therm_range=therm_range))
    return temps


# Awaiting testing board for testing conversion of raw AD values to mA/Volts
def get_aux_loop_single(aux_loop, mode):
    '''Accepts Inputs 1-4, in modes amps or volts'''
    # Aux 5, 6, 7, and 8 can be set to Voltage or mA
    # Human-readable Aux_loop numbers parsed to i2c byte value.
    aux_loop = aux_loop - 1
    if mode == 'amps':
        aux_loop = aux_loop
    if mode == 'volts':
        aux_loop = aux_loop + 4

    # Setup IO Mux
    global state_array
    # Inhibit Thermistor Read
    state_array[2] = 0x20  # Inihibits Thermistor Read
    # Setup 4-2mA Loop / Volt Reads
    state_array[3] = modify_byte(byte_value=state_array[3], low_nibble=0x01)
    # Select Aux Input
    state_array[4] = modify_byte(byte_value=state_array[4], low_nibble=aux_loop)
    # Read AD
    set_mux(command=state_array)
    aux_loop_raw = read_ad()
    x = aux_loop_raw
    reset_mux()
    return x
