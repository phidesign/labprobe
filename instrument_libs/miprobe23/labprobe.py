#!/usr/bin/env python3
import schedule
from b23_i2c import init_b23, get_all_b23t
from mp_basic import init_files, init_keys, timestamp, dict_to_csv, upload_data, xprint, time
from sht20 import sht20_temp
import threading

init_files()
api_key, api_url = init_keys()
init_b23(device_address=0x0a, sample_milliseconds=20)

# File lock for preventing data corruption from multiple schedules.
file_lock = threading.Lock()

# Cloud data lock for experimentation.
cloud_lock = threading.Lock()

# Memory lock for preventing data corruptions from multiple threads.
memory_lock = threading.Lock()

# Schedule for reading measurements of sensors
read_schedule = schedule.Scheduler()
read_lock = threading.Lock()

# Schedule for logging data to file and cloud
log_schedule = schedule.Scheduler()

# Memory for data queues
measurement_data = []


# Customized for experiment setup, gets timestamps, B23T sensor data, SHT20 sensor data, etc.
# Combines these measurements, and returns them.
def read_loop():
    measurements = {}
    ts_data = timestamp('MST')
    read_lock.acquire()
    miprobe_data = get_all_b23t(names=['Malbec', 'Tempranillo', 'Pinot Noir'], mode=2)
    sht20_data = sht20_temp(0x38, precision=2, temp_name="Lab_Temp", humidity_name="Lab_Humidity")
    read_lock.release()
    measurements = {**measurements, **ts_data}
    measurements = {**measurements, **miprobe_data}
    measurements = {**measurements, **sht20_data}
    return measurements


# Rename these "init" functions
def reading_job(debug=False):
    if debug is True:
        xprint("I'm running on thread %s" % threading.current_thread())
    measurements = read_loop()
    xprint(measurements)
    memory_lock.acquire()
    measurement_data.append(measurements)
    memory_lock.release()


# Rename these "init" functions
def logging_job(debug=False):
    if debug is True:
        xprint("I'm running on thread %s" % threading.current_thread())
    global measurement_data
    memory_lock.acquire()
    memory_items = len(measurement_data)
    xprint("In Memory: " + str(memory_items))
    logging_queue = measurement_data
    measurement_data = []
    memory_lock.release()
    queued_items = len(logging_queue)
    file_queue = queued_items
    cloud_queue = queued_items
    xprint("Queued: Items")
    for i in logging_queue:
        file_lock.acquire()
        dict_to_csv(data=i, filename="Data.csv")
        file_lock.release()
        file_queue = file_queue - 1
        xprint("Logged data to disk." + " " + str(file_queue) + " Items remaining.")
        try:
            cloud_lock.acquire()
            upload_data(data=i, experiment_id="B23T_Pinot", api_key=api_key, api_url=api_url)
            cloud_lock.release()
            cloud_queue = cloud_queue - 1
            xprint("Successfully uploaded data." + " " + str(cloud_queue) + " Items remaining.")
        except Exception as e:
            xprint(e)
            dict_to_csv(data=i, filename="Unsent.csv")


thread1 = threading.Thread(target=reading_job)
thread2 = threading.Thread(target=logging_job)

thread1.start()
thread2.start()

read_schedule.every(1).seconds.do(reading_job)
log_schedule.every(10).seconds.do(logging_job)

xprint(read_schedule.get_jobs())
xprint(log_schedule.get_jobs())

while True:
    read_schedule.run_pending()
    log_schedule.run_pending()
    time.sleep(1)
