import time
import math
import os
from mp_basic import *

def Average(lst):
    return sum(lst) / len(lst)

# Micropython Specific Code
if os.uname()[0] == 'rp2':  # Anyone have a better idea?
    import machine
    # import struct

    sda = machine.Pin(14)
    scl = machine.Pin(15)
    i2c = machine.SoftI2C(sda=sda, scl=scl, freq=400000)

    class bus():
        def read_byte_data(addr, register):
            """ Read a single byte from register of device at addr
                Returns a single byte """
            return i2c.readfrom_mem(addr, register, 1)[0]

        def read_i2c_block_data(addr, register, length):
            """ Read a block of length from register of device at addr
                Returns a bytes object filled with whatever was read """
            return i2c.readfrom_mem(addr, register, length)

        def write_byte_data(addr, register, data):
            """ Write a single byte from buffer `data` to register of device at addr
                Returns None """
            # writeto_mem() expects something it can treat as a buffer
            data = bytes(data)
            return i2c.writeto_mem(addr, register, data)

        def write_i2c_block_data(addr, register, data):
            """ Write multiple bytes of data to register of device at addr
                Returns None """
            # writeto_mem() expects something it can treat as a buffer
            return i2c.writeto_mem(addr, register, data)

        def xprint(*args):
            '''Print Wrapper to add timestamp and hostname to print or tqdm.write() function.'''
            time.localtime()
            time_string = str(time[0]) + "-" + str(time[1]) + "-" + str(time[2]) + " " + str(time[3]) + ":" + str(time[4] + ":" + str(time[5] + "." + str(time[6])))
            timestamp = str("[" + time_string + "]")
            prefix = timestamp
            print(prefix, *args)

# End Micropython Specific Code
else:
    from smbus2 import SMBus
    from bitarray import bitarray
    from bitarray.util import ba2int
    from bitarray.util import int2ba

    bus = SMBus(1)
    # Timestamp/tqdm print wrapper, useful for psuedo-benchmarking

    def xprint(*args, **kwargs):
        '''Print Wrapper to add timestamp and hostname to print or tqdm.write() function.'''
        import datetime
        from tqdm import tqdm
        timestamp = str("[" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f") + "]")
        prefix = timestamp  # + hostname
        try:
            tqdm.write(prefix, *args, **kwargs)
        except Exception:
            print(prefix, *args, **kwargs)

    data_location = os.path.join(os.path.expanduser('~'), 'mp_data')
    if not os.path.exists(data_location):
        xprint("Creating" + str(os.path.expanduser('~')) + "/mp_data/ directory")
        os.makedirs(data_location)
    else:
        xprint("Found data folder at: " + str(data_location))


# You must initialize this function to use the B23T device.
def init_b23(device_address=0x0a, sample_milliseconds=40):
    ''''0x0a is a randomly assigned default.  Check your jumpers to confirm hex address number.'''

    # i2c INSTRUCTIONS:
    # 001 IDENTIFY 16 BIT MODEL ( 8 BIT MODEL, 8 BIT HDWR REV)
    # 002 IDENTIFY 16 BIT FIRMWARE REV ( 8 BIT FIRMWARE TYPE, 8 BIT REV)
    # 003 PERTINENT 16 BIT APP NOTE (IF ANY)
    # 010 RESET/REBOOT
    # 020 MEASURE HI Z NODE; -2.048 TO + 2.048 VOLTS : 0-40960 COUNTS , SYNTHESIZED 0.1 MV RES
    # 040 MEASURE THERMISTOR NODE;  0 TO +2.048 VOLTS : 0-40960 COUNTS , SYNTHESIZED 0.05 MV RES
    # 050 MEASURE 10 VOLT NODE;  0 TO +2.048 VOLTS : 0-40960 COUNTS , SYNTHESIZED 0.05 MV RES
    # 060 MEASURE 20 MA NODE;    0 TO +2.048 VOLTS : 0-40960 COUNTS , SYNTHESIZED 0.05 MV RES
    # 080* SET SHUNT VOLTAGE Argument: (00 00 = negative rail, 159 255 = positive rail) ~ Reading
    # 081* SET SHUNT Node Argument: (0 disables) (1-24 active)
    # 082* SET SHUNT Resistance Argument: (0 disables, 1 = direct, 2 = 2 MegOhms, 3 = 150K, 4 = 10K)
    # 083 Read shunted node without resetting shunting conditions.  Allows rapid series of reads.
    # 240 MEASURE DIAGNOSTICS
    # 242 LOAD NODES 1-21 WITH ALTERNATING DATA.  (REPLIES WITH 242 WHEN DONE)
    # 243 LOAD NODES 1-21 WITH INCREMENTING DATA. (REPLIES WITH 243 WHEN DONE)
    # 244 LOAD NODES 1-21 WITH RANDOM DATA (REPLIES WITH 244 WHEN DONE
    # 245 AUTO SCAN (FROM BYTE 3 NODE TO BYTE 4 NODE, ONE NODE PER REQUEST)

    # *Note: These settings are disrupted by normal reads. Use addr,083,0,0 to read w/out disruption
    # ARGUMENTS FOR INPUT MEASUREMENTS & SHUNTS:
    # 0-40960 SHUNT VOLTAGE (MV)  (THE D/A HAS ONLY 10 BIT RES BUT MATCHES THE RANGE MATCH HI Z.)
    # 0-21 HI Z INPUTS OR SHUNT NODES
    # 22-24 THERMISTOR NODES
    # 25  4-20 MA MEASUREMENT
    # 260-10 VOLT MEASUREMENT

    # ARGUMENTS FOR DIAGNOSTIC (240) MEASUREMENTS:
    # 205 GROUND OFFSET (0 TO +40960 counts Bipolar RANGE)(Should be ~~2048)
    # 210 D/A DIRECT OUTPUT ((0 TO +40960 counts monopolar RANGE)
    # 211 D/A SCALED OUTPUT (0 TO +40960 counts Bipolar RANGE)
    # 222 INSERT 2 ADDITIONAL LINEFEEDS IN USB OUTPUT IN I2C TEST MODE (ADDRESS 1).

    # TEST MODULE HARDWARE:
    # NODE      SIGNAL
    # 1 +1.250 V (W/R GND)
    # 2 -1.250 V (W/R GND)
    # 3 GROUND  ( ~ 0 IN MONOPOLAR MODE . ~ 20480 IN BIPOLAR MODE)
    # 4~21      FLOAT (FOR SELF DRIVEN DRIFT ETC TESTS)
    # 22        2.048 V (INTERNAL A/D REFERENCE. SHOULD READ FULL SCALE)
    # 23,24     10 K OHM “THERMISTORS”
    # 25        5 MA
    # 26        3.3 VOLTS

    # ALL NODES ARE DRIVEN WITH APPROXIMATELY 0.1% ACCURACY
    # EXCEPT NODES 25 AND 26 WHICH SHOULD BE ~1 NOMINAL

    # SUGGESTED TRIAL CONVERSION FACTORS:
    # NODES 1-21, MV = COUNT / 10  (SUBTRACT GROUND COUNT, OR ANODE/REFERENCE COUNT)
    # NODE 25,  COUNT / 2048 = MA
    # NODE 26,  COUNT / 5.04 = MV

    global dev_addr
    global sample_ms
    global commands

    dev_addr = device_address
    sample_ms = sample_milliseconds / 1000  # for time.sleep() functions.
    commands = {
        "Model_ID": 1,
        "Firmware_ID": 2,
        "Notes": 3,
        "Reset": 10,
        "High_Z_Node": 20,
        "Thermistor_Node": 40,
        "Aux_V": 50,
        "Aux_mA": 60,
        "Set_Shunt_Voltage": 80,
        "Set_Shunt_Node": 81,
        "Set_Shunt_Resistance": 82,
        "Read_Shunted_Node": 83,
        "Diagnostics": 240,
        "Alternating_High_Z_Data": 242,
        "Incrementing_High_Z_Data": 243,
        "Random_High_Z_Data": 244,
        "Auto_Scan": 245,
        "Setup": 250
    }
    message = "Initialized i2c settings for device address: " + str(hex(dev_addr)) + " at " + str(sample_milliseconds) + " sample rate."
    xprint(message)


def B23T_instruction(command, value):
    instruction = commands[command]
    command = [instruction, value]
    return command


def B23T_Setup(address=0x0a, mode="GET", watchdog="DEFAULT", sample_milliseconds="DEFAULT"):
    sample_ms=33
    if mode == "GET":
        lsb = bitarray([0,0,0,0,0,0,0,0]) # 0
    if mode == "FAST":
        lsb = bitarray([0,0,0,0,0,1,0,0]) # 4
        sample_ms = 8
    if mode == "MEDIUM":
        lsb = bitarray([0,0,0,0,1,0,0,0]) # 8
        sample_ms = 18
    if mode == "SLOW":
        lsb = bitarray([0,0,0,0,1,1,0,0]) # 12
        sample_ms = 33
    if sample_milliseconds != "DEFAULT":
        sample_ms = sample_milliseconds
    if watchdog != "DEFAULT":
        count = watchdog # Count = 2 times as many seconds as the watchdog value, 150 = 300 seconds
        msb = int2ba(count, 8)
    else:
        msb = bitarray([0,0,0,0,0,0,0,0])
    word = msb[4:] + lsb[4:8]
    xprint(word)
    command = ba2int(word)
    xprint(command)
    init_b23(address, sample_ms)
    bus.write_i2c_block_data(address, command, 250)
    
    

def get_Standard_Reference(reference=21):
    command = B23T_instruction("High_Z_Node", reference)
    bus.write_i2c_block_data(dev_addr, 20, command)
    time.sleep(sample_ms)
    raw_data = bus.read_i2c_block_data(dev_addr, 20, 2)
    value = (raw_data[0] * 256) + raw_data[1]
    value = value / 10
    value = round(value, 2)
    return value


def i2c_oversampling_high_z(node, debug_samples, debug_threshold):
    samples = []
    for i in range(debug_samples):
        raw_data = bus.read_i2c_block_data(dev_addr, 20, 2)
        value = (raw_data[0] * 256) + raw_data[1]
        value = value / 10
        value = round(value, 2)
        samples.append(value)
        time.sleep(sample_ms)
    for i in samples:
        if i == 0.0:
            xprint("Node ", str(node), " Values: ", str(i))
    for i in samples:
        if float(i) < float(debug_threshold):
            xprint("Raw Value below debug threshold on node: ", str(node), " Value: ",i)
            samples.remove(i)
    value = Average(samples)
    return value


def get_High_Z_raw(node, debug=False, debug_samples=100, debug_min=300, debug_max=1350):
    command = B23T_instruction("High_Z_Node", node)
    bus.write_i2c_block_data(dev_addr, 20, command)
    time.sleep(sample_ms)
    if debug == False:
        raw_data = bus.read_i2c_block_data(dev_addr, 20, 2)
        value = (raw_data[0] * 256) + raw_data[1]
        value = value / 10
        value = round(value, 2)
    if debug == True:
        raw_data = bus.read_i2c_block_data(dev_addr, 20, 2)
        value = (raw_data[0] * 256) + raw_data[1]
        value = value / 10
        value = round(value, 2)
        time.sleep(sample_ms)
        tries = debug_samples
        while True:
            if float(debug_min) <= float(value) <= float(debug_max):
                break
            else:
                try:
                    command = B23T_instruction("High_Z_Node", node)
                    bus.write_i2c_block_data(dev_addr, 20, command)
                    time.sleep(sample_ms)
                    tries = tries - 1
                    xprint("Node ", str(node), " Values: ", str(value), " Tries remaining: ", str(tries))
                    raw_data = bus.read_i2c_block_data(dev_addr, 20, 2)
                    value = (raw_data[0] * 256) + raw_data[1]
                    value = value / 10
                    value = round(value, 2)
                    if tries == 0:
                        value = 10000
                        break
                except:
                    xprint("Exhausted Tries...")
                    value = 10000
                    break
    return value


def get_High_Z(node, reference=21):
    command = B23T_instruction("High_Z_Node", node)
    bus.write_i2c_block_data(dev_addr, 20, command)
    time.sleep(sample_ms)
    raw_data = bus.read_i2c_block_data(dev_addr, 20, 2)
    value = (raw_data[0] * 256) + raw_data[1]
    value = value / 10
    reference = get_Standard_Reference()
    value = value - reference
    value = round(value, 2)
    return value


def get_Aux_mA():
    command = B23T_instruction("Aux_mA", 25)
    bus.write_i2c_block_data(dev_addr, 60, command)
    time.sleep(sample_ms)
    raw_data = bus.read_i2c_block_data(dev_addr, 20, 2)
    value = (raw_data[0] * 256) + raw_data[1]
    value = value / 2048
    value = round(value, 2)
    return value


def get_Aux_V():
    command = B23T_instruction("Aux_V", 26)
    bus.write_i2c_block_data(dev_addr, 50, command)
    time.sleep(sample_ms)
    raw_data = bus.read_i2c_block_data(dev_addr, 20, 2)
    value = (raw_data[0] * 256) + raw_data[1]
    value = (value / 5.04) / 1000
    value = round(value, 2)
    return value


def convert_pH(mV):
    pH = 7 - (mV / 57.14)
    return pH


def steinhart_equation(thermistor_raw, beta, T, ohms):
    # Equation Coefficients
    T = T
    r = ohms / ((65535 / thermistor_raw) - 1)
    steinhart = math.log(r / ohms) / beta
    steinhart = steinhart + 1.0 / (T + 273.15)
    steinhart = (1.0 / steinhart) - 273.15
    steinhart = round(steinhart, 2)
    return steinhart


def get_temps(ohms=10000, T=25.0, beta=3950.0):
    temps = {}
    for i in range(3):
        node = 22 + i
        thermistor = 1 + i
        name = "Temp" + str(thermistor)
        command = B23T_instruction("Thermistor_Node", node)
        bus.write_i2c_block_data(dev_addr, 20, command)
        time.sleep(sample_ms)
        raw_data = bus.read_i2c_block_data(dev_addr, 20, 2)
        value = (raw_data[0] * 256) + raw_data[1]
        value = steinhart_equation(value, ohms, T, beta)
        temps[name] = round(value, 2)
    return temps


def Mode_1_High_Z(precision=2):
    reference = get_Standard_Reference()
    data = {
        "N01": round(get_High_Z_raw(1) - reference, precision),
        "N02": round(get_High_Z_raw(2) - reference, precision),
        "N03": round(get_High_Z_raw(3) - reference, precision),
        "N04": round(get_High_Z_raw(4) - reference, precision),
        "N05": round(get_High_Z_raw(5) - reference, precision),
        "N06": round(get_High_Z_raw(6) - reference, precision),
        "N07": round(get_High_Z_raw(7) - reference, precision),
        "N08": round(get_High_Z_raw(8) - reference, precision),
        "N09": round(get_High_Z_raw(9) - reference, precision),
        "N10": round(get_High_Z_raw(10) - reference, precision),
        "N11": round(get_High_Z_raw(11) - reference, precision),
        "N12": round(get_High_Z_raw(12) - reference, precision),
        "N13": round(get_High_Z_raw(13) - reference, precision),
        "N14": round(get_High_Z_raw(14) - reference, precision),
        "N15": round(get_High_Z_raw(15) - reference, precision),
        "N16": round(get_High_Z_raw(16) - reference, precision),
        "N17": round(get_High_Z_raw(17) - reference, precision),
        "N18": round(get_High_Z_raw(18) - reference, precision),
        "N19": round(get_High_Z_raw(19) - reference, precision),
        "N20": round(get_High_Z_raw(20) - reference, precision)
    }
    return data


def Mode_2_High_Z(shared_reference=False, precision=2, debug=False):
    '''Shared reference modes supported = True, False, ORP, and pH'''
    '''Default precision = 2 decimals places of numeric precision'''
    if shared_reference is True:
        reference = get_Standard_Reference()
        orp_reference = reference
        ph_reference = reference
    if shared_reference is False:
        reference = get_Standard_Reference()
        orp_reference = get_High_Z_raw(17)
        ph_reference = get_High_Z_raw(19)
    if shared_reference == "ORP":
        reference = get_High_Z_raw(17)
        orp_reference = reference
        ph_reference = reference
    if shared_reference == "pH":
        reference = get_High_Z_raw(19)
        orp_reference = reference
        ph_reference = reference

    if debug == False:
        data = {
            "N01": round(get_High_Z_raw(1) - reference, precision),
            "N02": round(get_High_Z_raw(2) - reference, precision),
            "N03": round(get_High_Z_raw(3) - reference, precision),
            "N04": round(get_High_Z_raw(4) - reference, precision),
            "N05": round(get_High_Z_raw(5) - reference, precision),
            "N06": round(get_High_Z_raw(6) - reference, precision),
            "N07": round(get_High_Z_raw(7) - reference, precision),
            "N08": round(get_High_Z_raw(8) - reference, precision),
            "N09": round(get_High_Z_raw(9) - reference, precision),
            "N10": round(get_High_Z_raw(10) - reference, precision),
            "N11": round(get_High_Z_raw(11) - reference, precision),
            "N12": round(get_High_Z_raw(12) - reference, precision),
            "N13": round(get_High_Z_raw(13) - reference, precision),
            "N14": round(get_High_Z_raw(14) - reference, precision),
            "N15": round(get_High_Z_raw(15) - reference, precision),
            "N16": round(get_High_Z_raw(16) - reference, precision),
            "ORP": round(get_High_Z_raw(18) - orp_reference, precision),
            "pH": round(convert_pH(get_High_Z_raw(20) - ph_reference), precision),
        }

    if debug == True:
        data = {
            "N01": round(get_High_Z_raw(1, debug=True) - reference, precision),
            "N02": round(get_High_Z_raw(2, debug=True) - reference, precision),
            "N03": round(get_High_Z_raw(3, debug=True) - reference, precision),
            "N04": round(get_High_Z_raw(4, debug=True) - reference, precision),
            "N05": round(get_High_Z_raw(5, debug=True) - reference, precision),
            "N06": round(get_High_Z_raw(6, debug=True) - reference, precision),
            "N07": round(get_High_Z_raw(7, debug=True) - reference, precision),
            "N08": round(get_High_Z_raw(8, debug=True) - reference, precision),
            "N09": round(get_High_Z_raw(9, debug=True) - reference, precision),
            "N10": round(get_High_Z_raw(10, debug=True) - reference, precision),
            "N11": round(get_High_Z_raw(11, debug=True) - reference, precision),
            "N12": round(get_High_Z_raw(12, debug=True) - reference, precision),
            "N13": round(get_High_Z_raw(13, debug=True) - reference, precision),
            "N14": round(get_High_Z_raw(14, debug=True) - reference, precision),
            "N15": round(get_High_Z_raw(15, debug=True) - reference, precision),
            "N16": round(get_High_Z_raw(16, debug=True) - reference, precision),
            "ORP": round(get_High_Z_raw(18, debug=True) - orp_reference, precision),
            "pH": round(convert_pH(get_High_Z_raw(20) - ph_reference), precision)
        }
        debug_data = {
            "N01_raw": round(get_High_Z_raw(1, debug=True), precision),
            "N02_raw": round(get_High_Z_raw(2, debug=True), precision),
            "N03_raw": round(get_High_Z_raw(3, debug=True), precision),
            "N04_raw": round(get_High_Z_raw(4, debug=True), precision),
            "N05_raw": round(get_High_Z_raw(5, debug=True), precision),
            "N06_raw": round(get_High_Z_raw(6, debug=True), precision),
            "N07_raw": round(get_High_Z_raw(7, debug=True), precision),
            "N08_raw": round(get_High_Z_raw(8, debug=True), precision),
            "N09_raw": round(get_High_Z_raw(9, debug=True), precision),
            "N10_raw": round(get_High_Z_raw(10, debug=True), precision),
            "N11_raw": round(get_High_Z_raw(11, debug=True), precision),
            "N12_raw": round(get_High_Z_raw(12, debug=True), precision),
            "N13_raw": round(get_High_Z_raw(13, debug=True), precision),
            "N14_raw": round(get_High_Z_raw(14, debug=True), precision),
            "N15_raw": round(get_High_Z_raw(15, debug=True), precision),
            "N16_raw": round(get_High_Z_raw(16, debug=True), precision),
            "ORP_raw": round(get_High_Z_raw(18, debug=True), precision),
            "pH_raw": round(convert_pH(get_High_Z_raw(20)), precision),
            "Reference": round(reference, precision)
        }
        data = {**data, **debug_data}
    return data


def Mode_3_High_Z(
    replicate_names=["Set_A", "Set_B", "Set_C"],
    precision=2,
    shared_reference="pH",
    fanout="B23E1R",
    temperature=False
):
    if fanout == "B23E1R": # check if we actually need different fanout types for the Mode3 function.
        if shared_reference == "ORP":
            set_a_orp_neg = get_High_Z_raw(21)
            set_a_ph_neg = set_a_orp_neg
            set_a_reference = set_a_orp_neg

            set_b_orp_neg = get_High_Z_raw(13)
            set_b_ph_neg = set_b_orp_neg
            set_b_reference = set_b_orp_neg

            set_c_orp_neg = get_High_Z_raw(18)
            set_c_ph_neg = set_c_orp_neg
            set_c_reference = set_c_orp_neg

        if shared_reference == "pH":
            set_a_ph_neg = get_High_Z_raw(11)
            set_a_orp_neg = set_a_ph_neg
            set_a_reference = set_a_ph_neg

            set_b_ph_neg = get_High_Z_raw(15)
            set_b_orp_neg = set_b_ph_neg
            set_b_reference = set_b_ph_neg

            set_c_ph_neg = get_High_Z_raw(19)
            set_c_orp_neg = set_c_ph_neg
            set_c_reference = set_c_ph_neg

        pH_a = round(get_High_Z_raw(12) - set_a_ph_neg, precision)

        data_a = {
            "N01": round(get_High_Z_raw(1) - set_a_reference, precision),
            "N02": round(get_High_Z_raw(2) - set_a_reference, precision),
            "N03": round(get_High_Z_raw(3) - set_a_reference, precision),
            "pH_mV":  round(pH_a, precision),
            "pH":  round(convert_pH(pH_a), precision),
            "ORP": round(get_High_Z_raw(10) - set_a_orp_neg, precision)
        }

        pH_b = round(get_High_Z_raw(16) - set_b_ph_neg, precision)

        data_b = {
            "N01": round(get_High_Z_raw(4) - set_b_reference, precision),
            "N02": round(get_High_Z_raw(5) - set_b_reference, precision),
            "N03": round(get_High_Z_raw(6) - set_b_reference, precision),
            "pH_mV": pH_b,
            "pH": round(convert_pH(pH_b), precision),
            "ORP": round(get_High_Z_raw(13) - set_b_orp_neg, precision)
        }

        pH_c = round(get_High_Z_raw(20) - set_c_ph_neg, precision)

        data_c = {
            "N01": round(get_High_Z_raw(7) - set_c_reference, precision),
            "N02": round(get_High_Z_raw(8) - set_c_reference, precision),
            "N03": round(get_High_Z_raw(9) - set_c_reference, precision),
            "pH_mV": pH_c,
            "pH": round(convert_pH(pH_c), precision),
            "ORP": round(get_High_Z_raw(17) - set_c_orp_neg, precision)
        }

        if temperature is True:
            temps = get_temps()
            data_a['Temp'] = temps['Temp1']
            data_b['Temp'] = temps['Temp2']
            data_c['Temp'] = temps['Temp3']

        data = {
            replicate_names[0]: data_a,
            replicate_names[1]: data_b,
            replicate_names[2]: data_c,
            "Metadata": {
                "Fanout": fanout,
                "Shared Reference Mode": shared_reference
            }
        }
        return data
    else:
        print("Check Fanout Board.") # ignore this for now


def get_all_b23t(names=["Set_A", "Set_B", "Set_C"], mode=0, aux_mA=(4, 20), aux_V=(0, 10)):
    data = {}
    aux_mA = {"Aux_mA": get_Aux_mA()}
    aux_V = {"Aux_V": get_Aux_V()}
    if mode == 0:
        miprobe_data = Mode_1_High_Z()
        temp_data = get_temps()
        data = {**data, **temp_data}
        data = {**data, **aux_mA}
        data = {**data, **aux_V}
        return data
    if mode == 1:
        miprobe_data = Mode_2_High_Z()
        temp_data = get_temps()
        data = {**data, **temp_data}
        data = {**data, **aux_mA}
        data = {**data, **aux_V}
        return data
    if mode == 2:
        miprobe_data = Mode_3_High_Z(replicate_names=names, temperature=True, shared_reference="pH")
        data = {**miprobe_data, **aux_mA}
        data = {**miprobe_data, **aux_V}
        return data