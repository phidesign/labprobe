from fastapi import FastAPI
import os
import threading
from mp_basic import *

data_lock = threading.Lock()

app = FastAPI

@app.get('/miprobe/latest/json')
def check_last_reading():
    xprint("Received latest reading request.  Sending last known readings.")
    data_lock.acquire()
    data = cloud_datas
    data_lock.release()
    return data

@app.get('/miprobe/query/json')
def query_board():
    xprint("Received Query Request.  Querying Sensors and sending new measurements.")
    experimentDict = settings
    board = str(settings['type'])
    experimentName = settings['experimentname']
    queryCommand = settings['query']
    localTZ = settings['timezone']
    errorFilename = str("Error-" + str(experimentName) + str(".csv"))
    rawFilename = str(experimentName).replace(' ', '_') + str("-Data") + str(".csv")
    offlineMode = settings['offline']
    # use different function if using relays
    relay_status = ""
    #experiment_loop(board, experimentName, queryCommand, localTZ, experimentDict, api_dict, errorFilename, rawFilename, offlineMode, relay_status)
    data = cloud_data  # Global from mp.experiment_loop()
    return data

@app.get('/miprobe/dump/json')
def dump_json():
    xprint("Received JSON Dump request. Sending all known readings.")
    rawFilename = str(settings['experimentname']).replace(' ', '_') + str("-Data") + str(".csv")
    data = csv_to_json(rawFilename)
    return data

@app.get('/miprobe/config')
def config():
    xprint("Received Configuration Request.  Sending current experiment configuration.")
    data = read_config(os.path.join(os.path.expanduser('~'), 'mp_data/', 'latest_config.json'))
    return data

@app.get('/alicat/query/json')
def query_alicat():
    xprint("Received Configuration Request.  Sending current experiment configuration.")
    data = read_config(os.path.join(os.path.expanduser('~'), 'mp_data/', 'latest_config.json'))
    return data

@app.put('/relay/{relay}')
def relay_control():
    data = 