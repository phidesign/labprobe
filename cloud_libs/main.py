from fastapi import FastAPI
from labprobe_cloud import *

app = FastAPI()

@app.get("/")
def read_root():
    data = get_pond_data()
    return data


@app.get("/ysi")
def read_ysi():
    data = get_pond_data()
    return data['ysi']

