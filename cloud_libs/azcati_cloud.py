from labprobe_cloud import *

default_pond_list = ['SPW9', 'SPW11', 'SPW13']

def get_pond_data(ponds_list=default_pond_list, start_date="2020-01-01 00:00:00", end_date='now', datasets=['ysi', 'lab_samples', 'harvest', 'weather']):
    data_dict = {}
    table_list = datasets

    if end_date == 'now':
        end_date = str(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

    if 'weather' in datasets:
        # Multi-thread this part
        table_list.remove('weather')
        data = query_table(key="LocationID", name='fieldsite', start=start_date, end=end_date, table_name='weather', time_column='Timestamp')
        data_dict['weather'] = pd.DataFrame(data)

    for dataset in table_list:
        data_dict[dataset] = pd.DataFrame()
        # Multi-thread this part.
        for pond in ponds_list:
            # Multi-thread this part.
            data = query_table(key="PondID", name=pond, start=start_date, end=end_date, table_name=dataset, time_column='Timestamp')
            data_dict[dataset] = pd.concat([data_dict[dataset], pd.DataFrame(data)], ignore_index=True)

    return data_dict

def get_pond_data2(ponds_list=default_pond_list, 
                  start_date="2020-01-01 00:00:00", 
                end_date='now', 
                datasets=aws_configuration['tables'], time_column='Timestamp'):
   
    data_dict = {}
    table_list = datasets

    if end_date == 'now':
        end_date = str(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

    if 'weather' in datasets:
        # Multi-thread this part.
        table_list.remove('weather')
        data = query_table(key=aws_configuration['environmental_metadata_hash_keys'], 
                           name='fieldsite', 
                           start=start_date, 
                           end=end_date, 
                           table_name=aws_configuration['environmental_metadata_table'], 
                           time_column=time_column)
        data_dict['weather'] = pd.DataFrame(data)

    for dataset in table_list:
        # Multi-thread this part.
        data_dict[dataset] = pd.DataFrame()
        for pond in ponds_list:
            # Multi-thread this part.
            data = query_table(key="PondID", name=pond, start=start_date, end=end_date, table_name=dataset, time_column=time_column)
            data_dict[dataset] = pd.concat([data_dict[dataset], pd.DataFrame(data)], ignore_index=True)

    return data_dict
